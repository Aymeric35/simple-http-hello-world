# Simple HTTP Hello World

Ce projet démarre un serveur HTTP basique qui répond aux requêtes HTTP sur le endpoint **/hello**.

Le serveur écoute sur le port **8181**. 

## Build du projet

Avec maven : `mvn package`. 

Le jar généré est disponible dans /target. Un jar auto-exécutable avec les dépendances du projet est disponible (sous le nom `*-jar-with-dependencies.jar`).

## Exemple

```
# Request
GET /hello

# Response
200 

Hello, world! It is currently 20:20:02.020
``` 

------------------------------------

## Création de l'image Docker

Pour créer l'image docker, utilisez la commande suivante :

```
docker build -t hello_world .
```

## Création du conteneur Docker

Pour démarrer un conteneur Docker, on utilisera cette commande :

```
docker container run -p 8181:8181 hello_world
```

## Test de la requête via Insomnia

Sur Insomnia, on exécutera une requête GET sur l'adresse `http://localhost:8181/hello` pour vérifier que l'application fonctionne correctement.