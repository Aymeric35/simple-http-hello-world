# Évaluation : Docker, Intégration continue, architecture

## Partie 1 : mettre en place l'intégration continue d'un petit projet java

Le projet suivant : [simple-http-hello-world · GitLab](https://gitlab.com/sylvain.revereault1/simple-http-hello-world) est un projet java simple et autonome (pas de base de données, un jar exécutable)

1. Mettre en place la conteneurisation de ce projet et rédiger la documentation qui permet de lancer le projet avec Docker. Cette doc doit être une section dans le README qui sera suivie pas à pas pour tester qu'on arrive bien à lancer le projet avec Docker.
2. Mettre en place une pipeline d'intégration continue avec Gitlab CI pour ce projet. Pour tester que cette pipeline fonctionne, on regardera dans votre projet Gitlab si la pipeline s'est exécutée correctement et si une image docker est disponible dans le registry gitlab.

**Bonus** : des points bonus vous seront accordés si l'image Docker produite n'embarque pas le code source.

## Partie 2 : Docker compose

Développer un fichier docker compose qui permet de démarrer votre projet de wordle en local, avec sa base de données. Le fichier devra démarrer plusieurs containers : 
 * L'application Wordle Java
 * La base de données
 * L'API Scrabble

Les trois containers doivent communiquer entre eux pour que l'application fonctionne sans autre dépendance.

**Bonus** : des points bonus vous seront accordés si vous créez dans docker compose 2 réseaux, afin d'isoler l'API Scrabble de la base de données.

## Partie 3 : étude de cas

En binôme, vous préparerez une présentation de 5 minutes (5 slides max) sur l'un des sujets suivants (un tirage au sort sera réalisé pour la composition szq binômes et l'attribution des sujets) : 
 * Le modèle OSI
 * Le principe d'encapsulation dans le réseau
 * L'authentification asymétrique
 * Le théorème CAP
 * Communication synchrone et asynchrone entre services
 * Principes de load balancing
